<?php

/* BEGIN CSS FILE */

global $lg_styles, $lg_scripts;

$lg_styles = [
	(object) array(
		"handle" => "lg-fonts",
		"src" => 'https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i|Roboto:300,300i,400,400i,700,700i',
		"dependencies" => [],
		"version" => false
	),
	(object) array(
		"handle" => "lg-style-child",
		"src" => get_stylesheet_directory_uri() . "/assets/dist/css/main.css",
		"dependencies" => [],
		"version" => filemtime(get_stylesheet_directory() . "/assets/dist/css/main.css")
	)
];

/* END CSS FILE */

/* BEGIN JS FILE */

$lg_scripts = [
	(object) array(
		"handle" => "lg-script-child",
		"src" => get_stylesheet_directory_uri() . "/assets/dist/js/main.js",
		"dependencies" => array('jquery'),
		"version" => filemtime(get_stylesheet_directory() . "/assets/dist/js/main.js")
	)
];
/* END JS FILE */

function lg_enqueue_styles_scripts() {

	global $lg_styles, $lg_scripts;

	/* INCLUDE MAIN */

	if($lg_styles && is_array($lg_styles)){
		foreach ($lg_styles as $key => $style) {

			wp_enqueue_style(
				$style->handle,
				$style->src,
				$style->dependencies,
				$style->version
			);
		}
	}

	if($lg_scripts && is_array($lg_scripts)){
		foreach ($lg_scripts as $key => $script) {
			wp_enqueue_script(
				$script->handle,
				$script->src,
				$script->dependencies,
				$script->version,
				true
			);

			wp_enqueue_script( $script->handle );
		}
	}

	/* END INCLUDE MAIN */

}

add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );

//enqueue theme style in editor
if($lg_styles && is_array($lg_styles)){
	foreach ($lg_styles as $key => $style) {
		add_editor_style($style->src);
	}
}

//enqueue editor style
add_editor_style(get_stylesheet_directory_uri()."/assets/dist/css/editor.css");



/*Remove wordpress emoji code*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/*Remove WP embed*/
function my_deregister_scripts(){
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/*Remove jquery migrate js*/
function remove_jquery_migrate($scripts)
{
	if (!is_admin() && isset($scripts->registered['jquery'])) {
		$script = $scripts->registered['jquery'];

		if ($script->deps) { // Check whether the script has any dependencies
			$script->deps = array_diff($script->deps, array(
				'jquery-migrate'
			));
		}
	}
}
add_action('wp_default_scripts', 'remove_jquery_migrate');